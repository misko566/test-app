# AWS FARGATE scheduled data fetcher


## Repository contains
- Dockerfile with python3 and scripts
- 3 scripts for docker (init, uploader, data extractor)
- terrafomr code for ECS, S3, Fargate, Cloudwatch

## Overview
- DOCKER with installation of python
  - init script with status informations
  - transactions sepparated to 4 steps (download, extrac, upload, clean)
  - used combination of bash and python
- Terraform
  - S3 code with static page
  - ECS code with vpc, nsg, iam, Fargate scheduled task
  - vars file with settings
  - code splitted into modules
  - integration to cloudwatch logs


## How to use

Set your ENV `AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY`
Edit  schedule_expression `terraform/terraform.tfvars` if you need

```bash>
cd terraform
terraform init
terraform apply
```

## Architecture
- CloudWatch periodically triggers ECS Fargate Task (cloudwatch_event_rule)
- Task will run container:
    1. Downloads dataset from https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/who_covid_19_situation_reports/who_covid_19_sit_rep_time_series/who_covid_19_sit_rep_time_series.csv
    2. Extract some data from dataset and put it into index.html
    3. Upload all data into S3

- example report for  data (`country_region='Czechia'`) can be shown on link: https://test-app-data.s3-website.eu-central-1.amazonaws.com
  after deployment

## More

- possibility to run docker anywhere
```bash>
docker run -e AWS_ACCESS_KEY_ID= -e AWS_SECRET_ACCESS_KEY= misko566/task-app:latest 
```
Custom ENV for docker `AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, BUCKET_NAME, DATA_FOLDER, DATA_SOURCE, DATA_COUNTRY  `


Query data (`country_region='Czechia'`) can be shown on link: https://fetch-data-lambda-output.s3.eu-central-1.amazonaws.com/index.html (simple html table)

## What I can do better
- everything :)
- Increase number of variables for customisation
- connect docker variables with terraform
- use better iam settings
- use only one python script in dockerfile


## Bonus solutions
- Use Kubernetes cron tasks
  - more stable
  - easier to use
  - dynamicaly scalable
  - faster to setup

- use CI/CD Jenkins or Gitlab
  - container will run on slave or runner
  

