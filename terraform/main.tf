provider "aws" {
    version = "~> 2.66"
    region  = "eu-central-1"
}

terraform {
    required_version = "~> 0.12.25"
}

module "s3" {
  bucket_name         = var.bucket_name
  source                      = "./modules/s3"
}


module "ecs" {
  bucket_name           = var.bucket_name
  schedule_expression   = var.schedule_expression
  source                = "./modules/ecs"
}

