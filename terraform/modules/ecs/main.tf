resource "aws_vpc" "main" {
  cidr_block = "172.31.0.0/16"
}


resource "aws_internet_gateway" "main" {
  vpc_id = aws_vpc.main.id
}


resource "aws_subnet" "private" {
  vpc_id            = aws_vpc.main.id
  cidr_block        = "172.31.0.0/24"
}


resource "aws_route_table" "private" {
  vpc_id = aws_vpc.main.id
}
 
resource "aws_route" "private" {
  route_table_id         = aws_route_table.private.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id         = aws_internet_gateway.main.id
}
 
resource "aws_route_table_association" "private" {
  subnet_id      = aws_subnet.private.id
  route_table_id = aws_route_table.private.id
}

 
resource "aws_security_group" "nsg_task" {
  name        = "task-app"
  vpc_id      = aws_vpc.main.id
}

resource "aws_security_group_rule" "nsg_task_egress_rule" {
  description = "Allows task to establish connections to all resources"
  type        = "egress"
  from_port   = "0"
  to_port     = "0"
  protocol    = "-1"
  cidr_blocks = ["0.0.0.0/0"]

  security_group_id = aws_security_group.nsg_task.id
}




resource "aws_ecs_cluster" "task" {
  name = var.bucket_name
}



resource "aws_iam_role" "ecs_task_execution_role" {
  name = "s3-ecsTaskExecutionRole"
 
   assume_role_policy = <<EOF
{
"Version": "2012-10-17",
 "Statement": [
   {
     "Action": "sts:AssumeRole",
     "Principal": {
       "Service": "ecs-tasks.amazonaws.com"
     },
     "Effect": "Allow",
     "Sid": ""
   }
 ]
}
 EOF
}

resource "aws_iam_role_policy_attachment" "ecs-task-execution-role-policy-attachment" {
  role       = aws_iam_role.ecs_task_execution_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}

resource "aws_iam_role_policy_attachment" "ecs" {
  role       = aws_iam_role.ecs_task_execution_role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
}





data "aws_iam_policy_document" "events_assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type = "Service"
      identifiers = ["events.amazonaws.com","ecs.amazonaws.com","ecs-tasks.amazonaws.com","s3.amazonaws.com"]
    }
  }
}


resource "aws_iam_role" "cloudwatch_events_role" {
  name = "task-app-cron-events"
  assume_role_policy = data.aws_iam_policy_document.events_assume_role_policy.json
}

resource "aws_iam_role_policy_attachment" "cron" {
  role       = aws_iam_role.cloudwatch_events_role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
}

resource "aws_iam_role_policy_attachment" "cron2" {
  role       = aws_iam_role.cloudwatch_events_role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonECS_FullAccess"
}

resource "aws_iam_role_policy_attachment" "cron3" {
  role       = aws_iam_role.cloudwatch_events_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}


 
resource "aws_ecs_task_definition" "service" {
  family                   = "task-app"
  network_mode             = "awsvpc"
  task_role_arn            = aws_iam_role.ecs_task_execution_role.arn
  execution_role_arn       = aws_iam_role.ecs_task_execution_role.arn
  requires_compatibilities = ["FARGATE"]
  cpu                      = 256
  memory                   = 512
  container_definitions    = file("files/task-app.json")
}


resource "aws_cloudwatch_event_rule" "scheduled_task" {
  name = "task-app-cron"
  description = "Runs fargate task"
  schedule_expression = var.schedule_expression
}

resource "aws_cloudwatch_event_target" "scheduled_task" {
  rule      = aws_cloudwatch_event_rule.scheduled_task.name
  target_id = "task-app-cron"
  arn       = aws_ecs_cluster.task.id
  role_arn  = aws_iam_role.cloudwatch_events_role.arn
  input     = jsonencode({})
  

  ecs_target {
    task_count = 1
    task_definition_arn = aws_ecs_task_definition.service.arn
    launch_type = "FARGATE"
    platform_version = "LATEST"


   
    network_configuration {
      assign_public_ip = true
      subnets = [aws_subnet.private.id]
      security_groups  = [aws_security_group.nsg_task.id]
    }
  }

  # allow the task definition to be managed by external ci/cd system
  lifecycle {
    ignore_changes = [
      ecs_target[0].task_definition_arn,
    ]
  }
}
