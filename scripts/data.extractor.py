import os
import re
import sys
import csv
import logging
import datetime
import urllib3
import boto3
import botocore
from botocore.exceptions import ClientError
from argparse import ArgumentParser



def extract_data(file_name, country, bucket):
    s3_client = boto3.client('s3')
            
    csv_file = csv.DictReader(open(file_name, "r", encoding="utf8"), delimiter=",")
    table = """
    <!DOCTYPE html>
     <html>
      <head>
      </head>
      <body>
        <table>
    """
    table += f"<tr>{''.join([f'<td>{header}</td>' for header in csv_file.fieldnames])}</tr>"

    #print(next(csv_file))
    for row in csv_file:
        if country == row['Country/Region']:
            table_row = "<tr>"
            #print(row)
            for field in csv_file.fieldnames:
                table_row += f"<td>{row[field]}</td>"
            table_row += "</tr>"
            table += table_row
    table += """
        </table>
      </body>
    </html>
    """
    
    

    if table:
        with open("index.html", "w") as fl:
            fl.write(table)





# Parsing argumentov entered at start
parser = ArgumentParser()
parser.add_argument("-f", "--file_name",
                    nargs=1,
                    type=str,
                    dest="file_name",
                    required=True,
                    help="filename")
parser.add_argument("-c", "--country",
                    nargs=1,
                    type=str,
                    dest="country",
                    required=True,
                    help="country")
parser.add_argument("-b", "--bucket",
                    nargs=1,
                    type=str,
                    dest="bucket",
                    required=True,
                    help="bucketname")

args = parser.parse_args()

extract_data(args.file_name[0], args.country[0], args.bucket[0])


