import os
import re
import sys
import csv
import logging
import datetime
import urllib3
import boto3
from botocore.exceptions import ClientError
from argparse import ArgumentParser


def upload_data(file_name, bucket, object_name=None):

    # If S3 object_name was not specified, use file_name
    if object_name is None:
        object_name = file_name

    # Upload the file
    s3_client = boto3.client('s3')
    try:
        if "index" in file_name:
            response = s3_client.upload_file(file_name, bucket, object_name, ExtraArgs={"ContentType": "text/html"})
        else:
            response = s3_client.upload_file(file_name, bucket, object_name)
    except ClientError as e:
        logging.error(e)
        return False
    return True


# Parsing argumentov entered at start
parser = ArgumentParser()
parser.add_argument("-f", "--file_name",
                    nargs=1,
                    type=str,
                    dest="file_name",
                    required=True,
                    help="filename")
parser.add_argument("-b", "--bucket",
                    nargs=1,
                    type=str,
                    dest="bucket",
                    required=True,
                    help="bucketname")
parser.add_argument("-o", "--object_name",
                    nargs=1,
                    type=str,
                    dest="object_name",
                    default="",
                    required=False,
                    help="folder")

args = parser.parse_args()

if not args.object_name:
    upload_data(args.file_name[0], args.bucket[0])
else:
    folder = "{}/{}".format(args.object_name[0], args.file_name[0])
    upload_data(args.file_name[0], args.bucket[0], folder)