#!/bin/bash

#set +x
DATE=$(date +'%F_%H:%M')
[ "$BUCKET_NAME" == "" ] && BUCKET_NAME=test-app-data
[ "$DATA_FOLDER" == "" ] && DATA_FOLDER=data
[ "$DATA_SOURCE" == "" ] && DATA_SOURCE=https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/who_covid_19_situation_reports/who_covid_19_sit_rep_time_series/who_covid_19_sit_rep_time_series.csv
[ "$DATA_COUNTRY" == "" ] && DATA_COUNTRY=Czechia

# Column number to place the status message
RES_COL="${RES_COL:-60}"
# Command to move out to the configured column number
MOVE_TO_COL="echo -en \\033[${RES_COL}G"
# Command to set the color to SUCCESS (Green)
SETCOLOR_SUCCESS="echo -en \\033[1;32m"
# Command to set the color to FAILED (Red)
SETCOLOR_FAILURE="echo -en \\033[1;31m"
# Command to set the color back to normal
SETCOLOR_NORMAL="echo -en \\033[0;39m"

# Function to print the SUCCESS status
OK_OUT() {
  if [ "${silent_flag}" != "true" ]
    then
      echo -n "["
      [ "${RC_INFO_COLOR}" != "false" ] && $SETCOLOR_SUCCESS
      echo -n $"  OK  "
      [ "${RC_INFO_COLOR}" != "false" ] && $SETCOLOR_NORMAL
      echo -n "]"
      echo -ne "\n"
  fi
  return 0
}

# Function to print the FAILED status message
FAIL_OUT() {
  if [ "${silent_flag}" != "true" ]
    then
      echo -n "["
      [ "${RC_INFO_COLOR}" != "false" ] && $SETCOLOR_FAILURE
      echo -n $"FAILED"
      [ "${RC_INFO_COLOR}" != "false" ] && $SETCOLOR_NORMAL
      echo -n "]"
      echo -ne "\n"
  fi
  return 1
}

# Function for simple output
STRING_OUT () {
  STRING4OUT=$1
      printf "%-${RES_COL}s" "${STRING4OUT} "
}

function download_data {
  STRING_OUT "[INFO] Downloading data ..."
  DOWNLOAD=$(wget -q -O covid_19_${DATE}.csv  $DATA_SOURCE)
  [ $? -eq 0 ] && OK_OUT || { FAIL_OUT ; echo ${DOWNLOAD} ; exit 1 ;}
}

function extrac_data
{
  STRING_OUT "[INFO] Exctracting data ..."
  # usage of python because there is no client in bash and auth is complicated
  EXTRAC=$(python3 data.extractor.py -f covid_19_${DATE}.csv -c $DATA_COUNTRY -b $BUCKET_NAME)
  [ $? -eq 0 ] && OK_OUT || { FAIL_OUT ; echo ${EXTRAC} ; exit 1 ;}
}


function upload_data
{
  STRING_OUT "[INFO] Uploading data ..."
  # usage of python because there is no client in bash and auth is complicated
  UPLOAD=$(python3 s3.upload.py -f covid_19_${DATE}.csv -b $BUCKET_NAME -o $DATA_FOLDER)
  [ $? -eq 0 ] && OK_OUT || { FAIL_OUT ; echo ${UPLOAD} ; exit 1 ;}

  STRING_OUT "[INFO] Uploading index ..."
  UPLOAD2=$(python3 s3.upload.py -f index.html -b $BUCKET_NAME)
  [ $? -eq 0 ] && OK_OUT || { FAIL_OUT ; echo ${UPLOAD2} ; exit 1 ;}
}



function clean_data {
  STRING_OUT "[INFO] Cleaning data ..."
  DELETE=$(rm covid_19_${DATE}.csv)
  DELETE=$(rm index.html)
  [ $? -eq 0 ] && OK_OUT || { FAIL_OUT ; echo ${DELETE} ; exit 1 ;}
}

#run
download_data

extrac_data

upload_data

clean_data

